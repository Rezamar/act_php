<!DOCTYPE HTML>
<html>
	<head>
	<link rel="shortcut icon" href="..\Images\Logo.ico">
		<meta charset="utf-8">
		<title>PagiTEC</title>
		<link href="Style.css" rel="stylesheet">
	</head>
	
	<body>
		<header>
		<ul class="horizontal">
		<img src="..\Images\Logo.png" id="logo">
		<li><a href="#">Inicio</a></li>
		<li><a href="#">Ubicación</a></li>
		<li><a href="#">Carreras</a></li>
		<li><a href="#">Contacto</a></li>
		</ul>
		</header>
		
		<div class="main">
            <div class="slides">
		    <img src="..\Images\Domo.png" alt="">
		    <img src="..\Images\Butacas.png" alt="">
		    <img src="..\Images\Ed1.png" alt="">
		    <img src="..\Images\Tec.png" alt="">
		    </div>
        </div>
        
        <div class="tabla">
        <table width="900" border="0" cellspacing="0" cellpadding="1"> 
            <tr bgcolor="#800000" align="center"> 
            <td><b><font color="#FFFFFF">Información de nuestro Tec</font></b></td> 
            </tr> 
            <tr bgcolor="#990033"> 
            <td> 
            <table width="100%" border="0" cellspacing="0" cellpadding="4"> 
            <tr bgcolor="#FFFFFF"> 
            <td>Misión Institucional.

Somos un Instituto Tecnológico Superior líder que atiende la demanda de educación superior tecnológica, integrante del Tecnológico Nacional de México y sectorizada en la Secretaría de Innovación Ciencia y Tecnología (SICyT) del Gobierno del Estado de Jalisco, que promueve la docencia, la investigación, la extensión y la vinculación de calidad, con un modelo educativo innovador centrado en competencias, con cuidado del medio ambiente y responsabilidad social.

Visión de Futuro.

El Instituto Tecnológico Superior de Zapopan tiene la posición de líder en el estado de Jalisco así como en el sistema tecnológico nacional por la excelencia de sus procesos educativo y de investigación científica, que trascienden en la formación de los mejores profesionistas e ingenieros del país, talentosos e innovadores, altamente competitivos a nivel mundial y pilares del desarrollo sostenido, sustentable y equitativo de México.

 

Valores y Principios que caracterizan nuestra institución

Innovación.

De manera continua buscamos introducir en todos los ámbitos del quehacer de nuestro Instituto, cambios que representen una mejora, un progreso y sobre todo un mayor valor para los receptores de nuestros servicios y productos (clientes internos y externos) y de todo nuestro ecosistema.

Respeto.

Reconocemos, aceptamos y apreciamos en cada persona, su valor propio y sus cualidades individuales como ser humano, así que siempre nos dirigimos a nuestro prójimo con atención y consideración, sin discriminación de cualquier índole.

Profesionalismo.

Como miembros del Instituto Tecnológico Superior de Zapopan, nos desempeñamos en nuestro trabajo con integridad y respeto, con esmero y dedicación, aplicando y desarrollando continuamente los conocimientos y las habilidades necesarias para realizar con calidad y productividad las funciones y responsabilidades propias de nuestro puesto.

Disciplina.

Conscientemente nos desempeñamos en forma ordenada y coordinada en el desempeño de nuestras actividades y en forma deliberada nos apegamos a las normas, políticas y procedimientos establecidos en la institución, evitando los atajos que aparentemente representan un menor esfuerzo físico, mental y emocional pero que conducen inevitablemente a resultados espurios o adversos.

Perseverancia.

Por convicción propia, decidimos emplear de manera enérgica y constante nuestras fuerzas físicas, intelectuales o morales para cumplir nuestras responsabilidades, compromisos adquiridos y alcanzar nuestras metas y objetivos.

Honestidad.

De manera consciente estamos determinados a actuar siempre con base a la verdad, evitando cualquier falsedad o componenda, y a ejercer la auténtica justicia dando a cada quién incluso a nosotros mismo lo que por derecho propio corresponde.</td> 
            </tr> 
            </table> 
            </td> 
            </tr> 
            </table> 
        </div>
		
		<script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
		<script src="..\js\source\jquery.slides.js"></script>
        <script>
        $(function(){
            $(".slides").slidesjs({
                play: {
                  active: true,
                    // [boolean] Generate the play and stop buttons.
                    // You cannot use your own buttons. Sorry.
                  effect: "slide",
                    // [string] Can be either "slide" or "fade".
                  interval: 5000,
                    // [number] Time spent on each slide in milliseconds.
                  auto: false,
                    // [boolean] Start playing the slideshow on load.
                  swap: true,
                    // [boolean] show/hide stop and play buttons
                  pauseOnHover: false,
                    // [boolean] pause a playing slideshow on hover
                  restartDelay: 2500
                    // [number] restart delay on inactive slideshow
                }
              });
            });
        </script>
        <div class="copyright">Copyright&copy; 2017 - Powered By Rezamar</div>


	</body>
</html>
