<!DOCTYPE HTML>
<HTML>
	<HEAD>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<TITLE>TecMM</TITLE>
	<link rel=stylesheet href="/css/bootstrap.min.css" type="text/css" media=screen>
	<script src="/js/jquery.js"></script>
	<script src="/js/bootstrap.min.js"></script>
	<nav class="navbar navbar-default" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
			data-target=".nabvar-ext1-collapse">
			<span class="sr-only">Desplegar</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#">Desarrollo Web</a>
		</div>
		
		<div class="collapse navbar-collapse navbar-ex1-collapse">
			<ul class="nav navbar-nav">
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						Parcial 1 <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="/Dweb/Index/Index.php">Pagina CSS/HTML</a></li>
					</ul>
				</li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
						Parcial 2 <b class="caret"></b>
					</a>	
					<ul class="dropdown-menu">
						<li><a href="#">Palindromo</a></li>					
						<li><a href="#">MayusMinus</a></li>
						<li><a href="#">Tu Signo</a></li>
						<li><a href="#">Examen Javascript</a></li>
						<li><a href="#">Formulario a Validar</a></li>
					</ul>
				</li>
				
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">
					Parcial 3 <b class="caret"></b>
					</a>
					<ul class="dropdown-menu">
						<li><a href="biblio.php">Biblioteca</a></li>
					</ul>
				</li>
			</ul>
		</div>
	</nav>
	</HEAD>
	
<BODY>
	<!--<H1>HOLA</H1> -->
</BODY>	
</HTML>
