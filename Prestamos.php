<!DOCTYPE HTML>
<HTML>
	<HEAD>
		<meta charset="utf-8">
		<meta name="viewport" content="width-device-width, initial-scale=1-0">
		<title>Sistema de Biblioteca</title>
		<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" media="screen">
		<script type="text/javascript" language="javascript" src="/js/bootstrap.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script type="text/javascript" language="javascript" src="/js/datatables.min.js"></script>
		<script type="text/javascript" language="javascript" src="/js/jquery.dataTables.js"></script>
		<script type="text/javascript" language="javascript" src="/css/jquery.dataTables.css"></script>
		
		<script>
			$(document).ready(function()
			{
				$('#example').dataTable(
				{	
					"ajax": "consulta.php",
						"columns":
						[
							{ "data": "matricula" },
							{ "data": "idobra" },
							{ "data": "Prestamo" },
							{ "data": "Entrega" }
						]
				});
			}) ;
		</script>
	</HEAD>
<BODY>
			<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-ext1-collapse">
				<span class="sr-only">Biblioteca</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>			
			</div>
			<a class="navbar-barra" href="#"></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left">
					<li><a href="biblio.php">Volver</a></li>
					<li><a href="Prestamos.php">Prestamo</a></li>
					<li><a href="#">Borrar</a></li>
					<li><a href="#">Borrar todo</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><b>Apartado de Prestamos</b></a></li>
					<li class="active"><a href="index.php">Indice de Actividades</a></li>
				</ul>
			</div>
			
			<div class="collapse navbar-collapse"></div>
		</nav>
	<form action="registrarpres.php" method="POST" name="form">
	<h5 align="left">nControl:</h5> 
	<input type="text" value="" name="Control" style="">
	<h5 align="left">idObra:</h5>
	<input type="text" value="" name="Idobra" style="">
	<h5 align="left">Fecha Inicio:(AA-MM-DD)</h5>
	<input type="text" value="" name="FechaI" style=""> 
	<h5 align="left">Fecha Entrega:(AA-MM-DD)</h5>
	<input type="text" value="" name="FechaE" style=""> 
	<input type="submit" value="Guardar">
	<br><br>
	
	<b><h4>Prestamos Actuales</h4></b>
	<table id="example" class="display" id="example" width="100%">
        <thead>
            <tr>
                <th>Control</th>
                <th>idObra</th>
                <th>Fecha Pidio</th>
                <th>Fecha Entrega</th>
            </tr>
        </thead>
    </table>
</BODY>
</HTML>
