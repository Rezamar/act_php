<!DOCTYPE HTML>
<HTML>
	<HEAD>
		<meta charset="utf-8">
		<meta name="viewport" content="width-device-width, initial-scale=1-0">
		<title>Sistema de Biblioteca</title>
		<link rel="stylesheet" href="/css/bootstrap.min.css" type="text/css" media="screen">
		<script type="text/javascript" language="javascript" src="/js/bootstrap.min.js"></script>
		<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
		<script type="text/javascript" language="javascript" src="/js/datatables.min.js"></script>
		<script type="text/javascript" language="javascript" src="/js/jquery.dataTables.js"></script>
		<script type="text/javascript" language="javascript" src="/css/jquery.dataTables.css"></script>
		
	</HEAD>
	
<BODY>
	<nav class="navbar navbar-default" role="navigation">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-ext1-collapse">
				<span class="sr-only">Biblioteca</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				</button>			
			</div>
			<a class="navbar-barra" href="#"></a>
			<div class="navbar-collapse collapse">
				<ul class="nav navbar-nav navbar-left">
					<li><a href="Usuarios.php">Volver</a></li>
					<li><a  href="Añausuarios.php">Añadir</a></li>
					<li><a href="#">Buscar</a></li>
					<li><a href="#">Borrar</a></li>
					<li><a href="#">Borrar todo</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="#"><b>Usuarios Registrados</b></a></li>
					<li class="active"><a href="index.php">Indice de Actividades</a></li>
				</ul>
			</div>
			
			<div class="collapse navbar-collapse"></div>
		</nav>
	<!--<script> MAURICIO 1/12/2018
			$(document).ready(function()
			{
				$('#example').dataTable(
				{	
					"ajax": "consultausu.php",
						"columns":
						[
							{ "data": "nombre" },
							{ "data": "apellidos" },
							{ "data": "nControl" }
						]
				});
			}) ;
			
		</script>-->
		
	<b><h4>Usuarios Actuales</h4></b>
	<table id="example" class="display" id="example" width="100%">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Apellido</th>
                <th>Numero de Control</th>
                <th><a href="#">Modificar</a></th>
                <th><a href="#">Eliminar</a></th>
            </tr>
            <?php
				include("conexion.php") ;
				
				$query = "SELECT * FROM db_usuarios" ;
				$resultado = $conexion->query($query) ;
				while($row=$resultado->fetch_assoc())
				{
								
				?>	
				 <tr>
					<td><?php echo $row['nombre'];?></td>
					<td><?php echo $row['apellidos'];?></td>
					<td><?php echo $row['nControl'];?></td>
					<td><a href="modificar.php?nControl=<?php echo $row['nControl']; ?>">Modificar</a></td>
					<td><a href="eliminar.php?nControl=<?php echo $row['nControl']; ?>">Eliminar</a></td>
				 </tr>
				<?php
				}
				?>
            
        </thead>
    </table>
    <form action="registrarusu.php" method="POST" name="form">
    <br><br><br>
    <h5 align="left">Nombre:</h5> 
	<input type="text" value="" name="Nombre" style="">
	<h5 align="left">Apellidos:</h5>
	<input type="text" value="" name="Apellidos" style="">
	<h5 align="left">N.Control:</h5>
	<input type="text" value="" name="Control" style=""> <br><br>
	<input type="submit" value="Guardar">
	</form>
</BODY>
</HTML>
